# `clp-vision`: Code for Working with Image Corpora

This repository collects code for working with (= preprocessing, extracting features from, etc) image corpora used by the CompLing-Potsdam group (formerly "dialogue systems group Bielefeld"). It started as a re-factorisation of the [code from our ACL 2016 paper](https://github.com/clp-research/image_wac).

A collection of notebooks that make use of the joint preprocessed format created here can be found in the [semantics with pictures](https://github.com/clp-research/sempix) repository.


*David Schlangen, 2019-04-07*
